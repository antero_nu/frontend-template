import App from '../../src/framework/app';

describe('App', () => {
  it('should throw error if no name given', () => {
    expect(() => {
      new App();
    }).to.throw('AppNameMissing');
  });

  it('should throw error when no parent given', () => {
    expect(() => {
      new App('App Name');
    }).to.throw('IllegalParent');
  });

  it('should throw error if parent is not a valid DOM element', () => {
    expect(() => {
      new App('App Name', 'not-valid-dom-element');
    }).to.throw('IllegalParent');
  });

  it('parent should contain child', () => {
    const body = document.querySelector('body');
    new App('Some App', body);
    body.querySelector('#some-app').should.exist;
  });
});