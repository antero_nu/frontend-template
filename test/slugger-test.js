import {makeSlug, camelize} from '../src/framework/slugger';

describe('Slugger', () => {
  it('should turn string into slug', () => {
    const slug = makeSlug('From This');
    slug.should.equal('from-this');
  });

  it('should only include legal characters', () => {
    const slug = makeSlug('This#! 67 a%& slug$');
    slug.should.equal('this-67-a-slug');
  });

  it('spaces in ends should be removed, not turned to dashes', () => {
    const slug = makeSlug('  From This ');
    slug.should.equal('from-this');
  });

  it('multiple consecutive spaces should be turned to only one dash', () => {
    const slug = makeSlug('From    This'); 
    slug.should.equal('from-this');
  });

  it('should do nothing if it is passed a slug', () => {
    const slug = makeSlug('this-is-already-a-slug');
    slug.should.equal('this-is-already-a-slug');
  })

  it('should turn a slug into camel case', () => {
    const camelCase = camelize('this-should-be-camel-case');
    camelCase.should.equal('thisShouldBeCamelCase');
  });

  it('if not passed a slug should turn it into slug first', () => {
    const camelCase = camelize('This should be camel case');
    camelCase.should.equal('thisShouldBeCamelCase');
  })
});