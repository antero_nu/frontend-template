import Component from '../../src/framework/component';
import template from './component-template.html';

export default class SubComponent extends Component {
  constructor(parent, options) {
    super(parent, options);
    this.template = template;
  }
}
