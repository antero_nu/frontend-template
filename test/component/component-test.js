import Component from '../../src/framework/component';
import SubComponent from './sub-component';

describe('Template tests', () => {
  it('template class should add itself to parent when rendered', () => {
    const templ = new SubComponent(document.getElementsByTagName('body')[0], {id: 'template-id-test'});
    templ.render();
    document.getElementById('template-id-test').should.exist;
  });

  it('should not add anything to dom if template base class used directly', () => {
    const newElem = document.createElement('div');
    document.getElementsByTagName('body')[0].appendChild(newElem);

    const templ = new Component(newElem, {});
    templ.render();

    newElem.innerHTML.should.be.empty;
  });
});
