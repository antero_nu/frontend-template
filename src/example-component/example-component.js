import Component from '../framework/component';
import template from './template.html';

export default class ExampleComponent extends Component {
  constructor(parent, options) {
    super(parent, options);
    this.template = template;
  }
}