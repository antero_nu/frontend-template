export function makeSlug(string) {
  let slug = string.toLowerCase().trim();
  slug = slug.replace(/\s+/g, '-');
  slug = slug.replace(/[^-_a-zA-Z0-9]/g, '');
  return slug;
}

/**
 * Make camel case version of a slug
 * 
 * Technically this works on all strings but should be used mainly for slugs.
 * This is lossy otherwise in the sense that it may lose characters.
 * 
 * @param {string} string 
 */
export function camelize(string) {
  let parts = makeSlug(string).split('-');
  const first = parts.shift();
  parts = parts.map((word) => {
    return word.charAt(0).toUpperCase() + word.slice(1);
  });
  
  return first + parts.join('');
}