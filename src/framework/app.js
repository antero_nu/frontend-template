import {makeSlug} from './slugger';

export default class App {
  constructor(name, parent) {
    if (!name) {
      throw new Error('AppNameMissing');
    }

    if (!parent || !parent.appendChild) {
      throw new Error('IllegalParent');
    }

    const app = document.createElement('div');
    app.setAttribute('id', makeSlug(name));
    parent.appendChild(app);
  }
}