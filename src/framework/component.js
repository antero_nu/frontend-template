/**
 * This class should be considered abstract (see if it works)
 */
export default class Component {
  constructor(parent, options = {}) {
    this.parent = parent;
    this.options = options;
    this.template = () => { return ''; };
  }

  /**
   * Render empty template rather than do nothing to save subclasses from
   * having to do it
   */
  render() {
    this.parent.innerHTML += this.template(this.options);
  }

  /*
    Additional render methods to be added soon, use when needed
    * render() [already exists]

    === All below should have defaults or what is their use? ===
    * render_as_form() [renders template_as_form.html]
    * render_as_table_row(columns = all) [renders template_as_table_row.html]
    * render_as_list_item() [renders template_as_list_item.html]
    * render_as_select_option() [renders template_as_select_option.html]
  */
}