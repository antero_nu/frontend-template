import 'babel-polyfill';
import elem from './elem';
import './styles.css';
import ExampleComponent from './example-component/example-component';

const el = elem();
const component = new ExampleComponent(el);
component.render();

const body = document.getElementsByTagName('body')[0];
body.appendChild(el);

const inputElem = document.createElement('input');
inputElem.setAttribute('id', 'input-field');
body.appendChild(inputElem);