# Frontend Template

This is a template for quickly getting started making frontend apps.

Currently it has no functionality to handle backend connections, no framework support etc, it's just what you need in term of packages to get started coding.

Run `npm install` in root folder and then `npm test` to see what it does currently.

See package.json for which packages are used. They will need to be updated or even changed if using this template in the future.

## Troubleshooting

Selenium webdriver tests may not work if geckodriver and/or chromedriver is not saved globally e.g. `npm i -g geckodriver chromedriver`.

Selenium tests do not currently exit and not sure if this is a good way of doing things at all, but they run and that is always something.

Not sure if all these packages are actually needed, but that is for laters. Most of them used only in dev anyway.
