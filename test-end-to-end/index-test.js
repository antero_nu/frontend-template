var webdriver = require('selenium-webdriver');
var firefox = require('selenium-webdriver/firefox');
var expect = require('chai').expect;

var options = new firefox.Options();
options.addArguments('-headless');

var driver = new webdriver.Builder()
  .forBrowser('firefox')
  .setFirefoxOptions(options)
  .build();

describe('Selenium', () => {
  beforeEach(() => {
    driver.manage().setTimeouts({implicit: 15000});
    driver.get('http://localhost:8080');
  });

  after(() => {
    driver.quit();
    
  });
  
  it('should add and find text in input field', (done) => {
    driver.findElement(webdriver.By.id('input-field')).then((elem) => {
      elem.sendKeys('This is something');
      driver.sleep(10).then(() => {
        elem.getAttribute('value').then((something) => {
          expect(something).to.equal('This is something');
          done();
        });
      });
    });
  });
});
