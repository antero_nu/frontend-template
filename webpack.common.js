const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const htmlWebpackPlugin = new HtmlWebpackPlugin({
  title: 'Purr'
});

const cleanWebpackPlugin = new CleanWebpackPlugin(['dist']);

module.exports = {
  entry: {
    app: './src/index.js'
  },
  plugins: [
    cleanWebpackPlugin,
    htmlWebpackPlugin
  ],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['babel-preset-env']
          }
        }
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.html$/,
        use: ['html-es6-template-loader']
      }
    ]
  }
};
